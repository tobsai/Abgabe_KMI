'use strict';

var images;
var mario;
var bg;
var obstacles = [];
var GROUNDLEVEL = 360;

/** @description Preloades all image files used in game. Returns a message on Concole that loading is finished.
 * @param None
 */
function preload() {
	var imgBackground = loadImage("assets/background.jpg");
	var imgMario = loadImage("assets/mario.jpg")
	var imgObstacle = loadImage("assets/obstacle.jpg")
    images = {
		background: imgBackground,
		mario: imgMario,
		obstacle: imgObstacle
	};
    console.log("end of preload");
}

/** @description Creates player screen with background and a new player.
 * @description Also randomly spawns 5 obstacles offscreen which move towards the player.
 * @param None
 */
function setup() {
    createCanvas(800, 500);
    mario = new Mario();
	bg = new Background();
	for(var i=0; i<5; i++)
		obstacles.push(new Obstacle());
    console.log("end of setup");
}

/** @description General gamecircle
 * @description Draws moved image of background and deletes obstacles when leaving the screen.
 * @description Also shows player character and checks if he collides with an obstacle.
 * @param None
 *
 */
function draw() {
	bg.draw();
	for(var i=0; i<obstacles.length; i++){
		obstacles[i].draw();
		if (obstacles[i].box.isCollission(mario.box)) {
			console.log("collission: " + mario.box.toString() + ", " + obstacles[i].box.toString());
			noLoop();
		}
	}
    mario.draw();
}


/** @description Checks if button is pressed.
 * @param None
 */
function keyPressed() {
  mario.jump();
}


/** @description Checks if mouse is pressed.
 * @param None
 */
function mousePressed() {
  loop();
}