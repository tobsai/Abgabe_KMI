'use strict';

function Mario() {
	var size = 50;
	this.box = new Box(50, GROUNDLEVEL, size, size);
	var velocityx = 0;

	this.right = function () {
		console.log("right");
		velocityx = 4;
		if (this.box.x > 805) {
			velocityx = -4
		}
	}

	this.left = function () {
		console.log("left");
		velocityx = -4;
		if (this.box.x < -5) {
			velocityx = 4;
		}
	}

	this.draw = function () {
		this.box.x = this.box.x + velocityx;
		image(images.mario, this.box.left(), this.box.top(), size, size);
	}
}
