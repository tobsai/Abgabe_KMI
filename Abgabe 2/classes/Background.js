function Background() {
	var posX = 0;
	
	this.draw = function () {
		posX = (posX - 0) % width;		/** changed to 0 movement */
		image(images.background, posX, 0);
	}
}
