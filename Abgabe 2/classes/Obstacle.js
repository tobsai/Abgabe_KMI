'use strict';
/* changed to make obstacles drop */
function Obstacle(){
	this.box = new Box(random(0, width), random(-300, 0), images.obstacle.width, images.obstacle.height);
	this.draw = function () {
		this.box.y = this.box.y + 2;
		if (this.box.y < 0)
			this.box.y = random(-300, 0);
			this.box.x = random(0,width);
		image(images.obstacle, this.box.left(), this.box.bottom(), this.box.right());
	}
}