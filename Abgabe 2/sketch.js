'use strict';


var b = p5.board('COM4', 'arduino');
var buttonL;
var buttonR;
var images;
var mario;
var bg;
var obstacles = [];
var GROUNDLEVEL = 360;
function preload() {
	var imgBackground = loadImage("assets/background.jpg");
	var imgMario = loadImage("assets/mario.jpg")
	var imgObstacle = loadImage("assets/obstacle.jpg")
	images = {
		background: imgBackground,
		mario: imgMario,
		obstacle: imgObstacle
	};
    console.log("end of preload");
}
function setup() {
    createCanvas(800, 500);
	mario = new Mario();
	bg = new Background();
	for(var i=0; i<8; i++)
		obstacles.push(new Obstacle());
	buttonL = b.pin(8, 'BUTTON LEFT');
	buttonR = b.pin(9, 'BUTTON RIGHT');
	buttonL.pressed (mario.left());
	buttonR.pressed (mario.right());
	console.log("end of setup");
}

function draw() {
	bg.draw();
	for(var i=0; i<obstacles.length; i++){
		obstacles[i].draw();
		if (obstacles[i].box.isCollission(mario.box)) {
			console.log("collission: " + mario.box.toString() + ", " + obstacles[i].box.toString());
			noLoop();
		}
	}
	mario.draw();
}

function mousePressed() {
	loop();
}